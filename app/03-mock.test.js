'use strict';

jest.mock('./lib');
const {external} = require('./lib');

const {
  callsExternalSync,
  callsExternalCb,
  callsExternalPromise,
  callsRecievedSync,
  callsRecievedCb,
  callsRecievedPromise,
} = require('./03-mock');

describe('patch external modules', () => {
  beforeEach(() => {
    external.mockReset();
  });

  it('mock a sync function', () => {
    external.mockReturnValue(1);
    expect(callsExternalSync(2)).toBe(3);
    expect(external).toBeCalledWith(2);
  });

  it('mock a function that calls back', done => {
    external.mockImplementation((cb) => {
      cb(null, 1);
    });
    callsExternalCb((err, value) => {
      expect(value).toBe(3);
      expect(external).toBeCalledWith(expect.any(Function), 2);
      done();
    }, 2);
  });

  it('mock a function that returns a promise', () => {
    external.mockResolvedValue(1);
    expect(callsExternalPromise(2)).resolves.toBe(3).then(() => {
      expect(external).toBeCalledWith(2);
    });
  });
});

describe('pass an async function', () => {
  const fn = jest.fn();
  beforeEach(() => {
    external.mockReset();
  });

  it('pass a sync function', () => {
    fn.mockReturnValue(1);
    expect(callsRecievedSync(fn, 2)).toBe(3);
    expect(fn).toBeCalledWith(2);
  });

  it('pass a function that calls back', done => {
    fn.mockImplementation((cb) => {
      cb(null, 1);
    });
    callsRecievedCb((err, value) => {
      expect(value).toBe(3);
      expect(fn).toBeCalledWith(expect.any(Function), 2);
      done();
    }, fn, 2);
  });

  it('pass a function that returns a promise', () => {
    fn.mockResolvedValue(1);
    expect(callsRecievedPromise(fn, 2)).resolves.toBe(3).then(() => {
      expect(fn).toBeCalledWith(2);
    });
  });
});
