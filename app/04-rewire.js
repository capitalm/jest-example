'use strict';

function exported(r) {
  return priv1() + r;
}

function priv1() {
  throw 'Not implemented';
}

module.exports = {
  exported,
};
