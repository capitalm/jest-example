'use strict';

function addSync(l, r) {
  return l + r;
}

function updateSync(l, r) {
  return Object.assign(l, r);
}

function addCallback(cb, l, r) {
  cb(null, l + r);
}

function addPromise(l, r) {
  return new Promise(resolve => {
    resolve(l + r);
  });
}

module.exports = {addSync, updateSync, addCallback, addPromise};
