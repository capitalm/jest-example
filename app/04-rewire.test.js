'use strict';

const rewire = require('rewire');
const subject = rewire('./04-rewire.js');

describe('Mock a private function', () => {
  const fn = jest.fn();
  let revert;
  let exported;

  beforeEach(() => {
    revert = subject.__set__('priv1', fn);
    exported = subject.__get__('exported');
    fn.mockReset();
  });

  afterEach(() => {
    revert();
  });

  it('test with a mocked private function', () => {
    fn.mockReturnValue(1);
    expect(exported(2)).toBe(3);
  });
});

