'use strict';

function external() {
  throw 'Not implemented';
}

module.exports = {
  external,
};
