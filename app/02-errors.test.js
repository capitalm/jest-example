'use strict';

const {
  willThrow,
  willCallbackError,
  willReject,
} = require('./02-errors');

describe('errors', () => {
  it('Throws an error', () => {
    return expect(() => {
      willThrow();
    }).toThrow('Throw error');
  });

  it('Calls cb with an error', done => {
    willCallbackError((err) => {
      expect(err).toBe('Callback error');
      done();
    });
  });

  it('Returns a promise that rejects with an error', () => {
    expect(willReject()).rejects.toBe('Reject error');
  });
});
