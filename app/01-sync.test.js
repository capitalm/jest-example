'use strict';

const {
  addSync,
  updateSync,
  addCallback,
  addPromise,
} = require('./01-expect');

describe('addAsync', () => {
  it('returns the sum of two numbers', () => {
    return expect(addSync(1, 2)).toBe(3);
  });
});

describe('updateSync', () => {
  it('compare objects with toEqual', () => {
    expect(updateSync({one: 1}, {two: 2})).not.toBe({one: 1, two: 2});
    return expect(updateSync({one: 1}, {two: 2})).toEqual({one: 1, two: 2});
  });
});

describe('addCallback', () => {
  it('calls cb with the sum of two numbers', done => {
    addCallback((err, value) => {
      expect(value).toBe(3);
      done();
    }, 1, 2);
  });
});

describe('addPromise', () => {
  it('returns a promise that resolves with the sum of two numbers', () => {
    return expect(addPromise(1, 2)).resolves.toBe(3);
  });
});
