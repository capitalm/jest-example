'use strict';

function willThrow() {
  throw 'Throw error';
}

function willCallbackError(cb) {
  cb('Callback error');
}

function willReject() {
  return new Promise((resolve, reject) => {
    reject('Reject error');
  });
}

module.exports = {
  willThrow,
  willCallbackError,
  willReject,
};
