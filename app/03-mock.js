'use strict';

const {external} = require('./lib');

function callsExternalSync(r) {
  const l = external(r);
  return l + r;
}

function callsExternalCb(cb, r) {
  external((err, l) => {
    cb(null, l + r);
  }, r);
}

function callsExternalPromise(r) {
  return external(r).then(l => {
    return l + r;
  });
}

function callsRecievedSync(fn, r) {
  const l = fn(r);
  return l + r;
}

function callsRecievedCb(cb, fn, r) {
  fn((err, l) => {
    cb(null, l + r);
  }, r);
}

function callsRecievedPromise(fn, r) {
  return fn(r).then(l => {
    return l + r;
  });
}

module.exports = {
  callsExternalSync,
  callsExternalCb,
  callsExternalPromise,
  callsRecievedSync,
  callsRecievedCb,
  callsRecievedPromise,
};
